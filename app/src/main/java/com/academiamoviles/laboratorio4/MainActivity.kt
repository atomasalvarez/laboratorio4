package com.academiamoviles.laboratorio4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialogo.*
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    var autoSeleccionado = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Lista de marcas de auto
        val auto:List<String> = listOf("Toyota","Nissan","Audi","Kia")
        //Vamos a poblar el spinner
        val arrayAdapter = ArrayAdapter(this,R.layout.style_item_spinner,auto)
        spAuto.adapter=arrayAdapter
        spAuto.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                autoSeleccionado = auto[position]
            }
        }
        btnConfirmar.setOnClickListener {
            val nombreCompleto =edtNombre.text.toString()
            val anio = edtAnio.text.toString()
            val descripcion = edtDescripcion.text.toString()
            val marcaAuto:Automovil=Automovil(nombreCompleto,autoSeleccionado,anio,descripcion)
            createDialogo(marcaAuto).show()
        }
    }
    fun createDialogo(autoElegido:Automovil):AlertDialog{
        val alertDialog:AlertDialog
        val builder=AlertDialog.Builder(this)
        val inflater= layoutInflater
        val view:View = inflater.inflate(R.layout.dialogo,null)
        builder.setView(view)
        val tvNombre:TextView = view.findViewById(R.id.tvNombre)
        val tvAnio:TextView=view.findViewById(R.id.tvAnio)
        val tvDescripcion:TextView=view.findViewById(R.id.tvDescripcion)
        val tvMarca:TextView = view.findViewById(R.id.tvMarca)
        val btnSalir:Button = view.findViewById(R.id.btnSalir)
        tvNombre.text = "Hola ${autoElegido.nombre}"
        tvAnio.text = "Auto del año ${autoElegido.anio}"
        tvMarca.text = "Marca del auto es ${autoElegido.marca}"
        tvDescripcion.text = "Su descripcion es ${autoElegido.descripcion}"
        alertDialog=builder.create()
        btnSalir.setOnClickListener {
            alertDialog.dismiss()
        }
        return alertDialog
    }
}